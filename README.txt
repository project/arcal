Prelude
------------
Arcal Module let you automatically publish any node to the calendar. Generating the calendar is based on, Giorgos Tsiledakis' Active Calendar, http://www.micronetwork.de/activecalendar/


Installation
------------

Copy arcal.module to your module directory and then enable on the admin
modules page.  Goto admin/settings/arcal to choose the Node types you wish to show in the calendar along with some other options.
http://yourwebsite/arcal

Author
------
safeen
safeen@madcap.nl
